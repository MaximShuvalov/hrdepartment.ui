import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';

class Menu extends React.Component {
    render() {
        return(
        <div class='general-menu'>
            <ButtonCustom text="Список всех сотрудников"/>
            <ButtonCustom text="Уволить сотрудника"/>
            <ButtonCustom text="Трудоустроить соискателя"/>
            <ButtonCustom text="Список всех отделов"/>
        </div>
        )
    }
}

class ButtonCustom extends React.Component {
    render() {
        return(
        <button class='menu-button'>{this.props.text}</button>
        )
    }
}

ReactDOM.render(
    <React.StrictMode>
        <Menu/>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
